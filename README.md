# Chords Editor

---

Steps to run the application:

1. run `npm install`

2. run `npm run start` and open: http://localhost:4200/

3. If you want, you can upload a song from the file: data/example.json

---

To create a production build run: `npm run build-prod` and you will find a builded project files in dist folder.

---

Thanks! 😉