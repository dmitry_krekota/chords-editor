import {Component} from '@angular/core';
import {ContentService} from './services/content.service';

@Component({
    selector: 'chords-editor',
    templateUrl: 'chords-editor.component.html'
})
export class ChordsEditorComponent {
    song: Widget;
    helpersEnabled: boolean = true;

    constructor(private contentService: ContentService) {
        let ceSong = localStorage.getItem('ceSong');
        if (ceSong) {
            this.song = JSON.parse(ceSong);
        } else {
            this.song = {
                name: 'song',
                properties: {author: 'Author', name: 'Name'},
                children: [],
                parent: null,
                id: '123456'
            };
        }
        this.contentService.initializeWidgetsTree(this.song);
    }

    onChange(event: any) {
        let files = event.srcElement.files;
        let file = files[0];
        let fr = new FileReader();
        fr.onload = (fileReaderProgressEvent: any) => {
            this.song = JSON.parse(fileReaderProgressEvent.target.result);
            this.contentService.initializeWidgetsTree(this.song);
        };
        fr.readAsText(file);
    }

    saveToFile() {
        this.contentService.downloadAsFile();
    }

    addLine() {
        this.contentService.executeAction(
            'create',
            {
                name: 'line',
                properties: {
                    content: 'Line...'
                },
                children: [],
                parent: this.song,
                id: Math.random()
                    .toString(36)
                    .substr(2, 6)
            },
            {
                index: this.song.children.length
            }
        );
    }

    toggleEditorHelpers() {
        this.helpersEnabled = !this.helpersEnabled;
    }
}
