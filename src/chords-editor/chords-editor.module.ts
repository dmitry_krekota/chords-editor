import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {ContentModule} from './content/content.module';
import {ChordsEditorRoutingModule} from './chords-editor-routing.module';
import {ChordsEditorComponent} from './chords-editor.component';

@NgModule({
    declarations: [ChordsEditorComponent],
    imports: [BrowserModule, ChordsEditorRoutingModule, ContentModule],
    providers: [],
    bootstrap: [ChordsEditorComponent]
})
export class ChordsEditorModule {}
