import {Component, Input} from '@angular/core';
import {ContentService} from '../services/content.service';

@Component({
    selector: 'ce-content',
    templateUrl: 'content.component.html'
})
export class ContentComponent {
    @Input() song: Widget;

    constructor(private contentService: ContentService) {}

    addAccord(songLine) {
        this.contentService.executeAction(
            'create',
            {
                name: 'accord',
                properties: {
                    content: 'Am',
                    offset: '0rem'
                },
                children: [],
                parent: songLine,
                id: Math.random()
                    .toString(36)
                    .substr(2, 6)
            },
            {
                index: 0
            }
        );
    }
}
