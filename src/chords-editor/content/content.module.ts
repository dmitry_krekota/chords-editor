import {NgModule} from '@angular/core';
import {ContentComponent} from './content.component';
import {BrowserModule} from '@angular/platform-browser';
import {EditorElementsModule} from '../editor-elements/editor-elements.module';

@NgModule({
    declarations: [ContentComponent],
    imports: [BrowserModule, EditorElementsModule],
    exports: [ContentComponent],
    providers: []
})
export class ContentModule {}
