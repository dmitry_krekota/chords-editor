import {NgModule} from '@angular/core';
import {WidgetComponent} from './widget.component';
import {BrowserModule} from '@angular/platform-browser';

@NgModule({
    declarations: [WidgetComponent],
    imports: [BrowserModule],
    exports: [WidgetComponent],
    providers: []
})
export class EditorElementsModule {}
