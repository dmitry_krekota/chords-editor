import {Component, Input, OnInit, OnChanges} from '@angular/core';
import {ContentService} from '../services/content.service';

@Component({
    selector: 'ce-widget',
    templateUrl: 'widget.component.html'
})
export class WidgetComponent implements OnInit, OnChanges {
    @Input() widget: Widget;
    @Input() property: string;
    @Input() removeDisabled: boolean;
    @Input() moveDisabled: boolean;
    content: string;
    dragHelper: HTMLElement;
    _onDocumentMouseUp: any;
    _onDocumentMouseMove: any;

    constructor(private contentService: ContentService) {}

    ngOnInit() {
        if (this.widget) {
            this.content = this.widget.properties[this.property];
        }
    }

    ngOnChanges() {
        if (this.widget) {
            this.content = this.widget.properties[this.property];
        }
    }

    onEdit(value: string) {
        if (this.content === value) {
            return;
        }
        this.content = value;
        this.contentService.executeAction('setProperties', this.widget, {
            [this.property]: this.content
        });
    }

    onDocumentMouseMove(event) {
        this.dragHelper.style.top = event.pageY + 'px';
        this.dragHelper.style.left = event.pageX + 'px';
        this.dragHelper.style.display = 'block';
        event.preventDefault();
    }

    onDocumentMouseUp(event) {
        let line = event.target.closest('.ce-song-line');
        if (line) {
            if (this.widget.name === 'accord') {
                let fontSize = 16;
                let offsetX = (event.pageX - line.getBoundingClientRect().left) / fontSize;
                this.contentService.executeAction('setProperties', this.widget, {
                    offset: `${offsetX.toFixed(2)}rem`
                });
                if (this.widget.parent.id !== line.getAttribute('id')) {
                    let newParent = this.widget.parent.parent.children.find(
                        (item) => item.id === line.getAttribute('id')
                    );
                    this.contentService.executeAction('move', this.widget, {
                        newParent: newParent,
                        index: 0
                    });
                }
            }
            if (this.widget.name === 'line') {
                if (this.widget.id !== line.getAttribute('id')) {
                    let targetIndex = this.widget.parent.children.findIndex(
                        (item) => item.id === line.getAttribute('id')
                    );
                    this.contentService.executeAction('move', this.widget, {
                        newParent: this.widget.parent,
                        index: targetIndex
                    });
                }
            }
        }
        this.destroyDragHelper();
    }

    activateDragHelper() {
        this.dragHelper = document.createElement('div');
        this.dragHelper.classList.add('ce-drag-helper');
        document.body.appendChild(this.dragHelper);
        document.body.classList.add('ce-drag-active');
        this._onDocumentMouseMove = this._onDocumentMouseMove || this.onDocumentMouseMove.bind(this);
        document.addEventListener('mousemove', this.onDocumentMouseMove.bind(this));
        this._onDocumentMouseUp = this._onDocumentMouseUp || this.onDocumentMouseUp.bind(this);
        document.addEventListener('mouseup', this._onDocumentMouseUp);
    }

    destroyDragHelper() {
        this.dragHelper.remove();
        document.body.classList.remove('ce-drag-active');
        document.removeEventListener('mousemove', this.onDocumentMouseMove.bind(this));
        document.removeEventListener('mouseup', this._onDocumentMouseUp);
    }

    startDrag() {
        this.activateDragHelper();
    }

    remove() {
        this.contentService.executeAction('remove', this.widget);
    }
}
