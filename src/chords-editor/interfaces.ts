interface Widget {
    name: string;
    properties: any;
    children: Array<Widget>;
    id: string;
    parent: Widget;
}
