import {Injectable} from '@angular/core';
import * as cloneDeep from 'lodash/cloneDeep';

@Injectable({
    providedIn: 'root'
})
export class ContentService {
    increment: number = 0;
    tree: Widget;

    actions: Object = {
        setProperties: (widget: Widget, properties: Object) => {
            widget.properties = {...widget.properties, ...properties};
        },
        create: (widget: Widget, properties: any) => {
            widget.parent.children.splice(properties.index, 0, widget);
        },
        remove: (widget: Widget) => {
            widget.parent.children.splice(widget.parent.children.findIndex((item) => item.id === widget.id), 1);
        },
        move: (widget: Widget, properties: any) => {
            if (widget.parent !== properties.newParent) {
                properties.newParent.children.splice(properties.index, 0, widget);
                widget.parent.children.splice(widget.parent.children.findIndex((item) => item.id === widget.id), 1);
                widget.parent = properties.newParent;
            } else {
                let newIndex = properties.index;
                let selfIndex = widget.parent.children.findIndex((item) => item.id === widget.id);
                if (newIndex !== selfIndex) {
                    let parentChildren = widget.parent.children;
                    while (selfIndex < 0) {
                        selfIndex += parentChildren.length;
                    }
                    while (newIndex < 0) {
                        newIndex += parentChildren.length;
                    }
                    if (newIndex >= parentChildren.length) {
                        let k = newIndex - parentChildren.length;
                        while (k-- + 1) {
                            parentChildren.push(undefined);
                        }
                    }
                    parentChildren.splice(newIndex, 0, parentChildren.splice(selfIndex, 1)[0]);
                }
            }
        }
    };

    executeAction(actionName: string, widget: Widget, properties?: Object) {
        this.actions[actionName](widget, properties);
        this.saveTreeToLocalStorage();
    }

    downloadAsFile() {
        let treeForSave = this.getWidgetsTreeForSave();
        let element = document.createElement('a');
        element.setAttribute(
            'href',
            'data:text/plain;charset=utf-8,' + encodeURIComponent(JSON.stringify(treeForSave))
        );
        element.setAttribute('download', `${treeForSave.properties.author} - ${treeForSave.properties.name}.json`);
        element.style.display = 'none';
        document.body.appendChild(element);
        element.click();
        document.body.removeChild(element);
    }

    cleanForSave(threeItem) {
        delete threeItem.id;
        delete threeItem.parent;
        if (threeItem.children && threeItem.children.length) {
            for (let i = 0; i < threeItem.children.length; i++) {
                this.cleanForSave(threeItem.children[i]);
            }
        }
    }

    getWidgetsTreeForSave() {
        let treeForSave = cloneDeep(this.tree);
        this.cleanForSave(treeForSave);
        return treeForSave;
    }

    saveTreeToLocalStorage() {
        let treeForSave = this.getWidgetsTreeForSave();
        localStorage.setItem('ceSong', JSON.stringify(treeForSave));
    }

    initializeWidgetsTree(widgetTreeItem: Widget, parent?: Widget) {
        widgetTreeItem.id =
            Math.random()
                .toString(36)
                .substr(2, 5) + this.increment++;
        if (parent) {
            widgetTreeItem.parent = parent;
        }
        if (widgetTreeItem.children && widgetTreeItem.children.length) {
            widgetTreeItem.children.forEach((item) => {
                this.initializeWidgetsTree(item, widgetTreeItem);
            });
        }
        if (!parent) {
            this.tree = widgetTreeItem;
            this.saveTreeToLocalStorage();
        }
    }
}
